using PPTest;
using System.Numerics;
using Xunit;

namespace Tests
{
    public class UnitTest
    {
        //1. For array["3", "-2"] should return "-2"
        //2. For array["5", "5", "4", "2"] should return "4"
        //3. For array["4", "4", "4"] should return -1 
        //   (duplicates are not considered as the second max)
        //4. For[] (empty array) should return -1.
        //5. For["2"] (array with one item) should return -1.
        //6. For["a", "1", "2", "b"] (array that contains string) should return -1.
        //6. For["d"] (array that contains string) should return -1.

        [Theory]
        //Default test cases
        [InlineData(new string[] { "3", "-2" }, "-2")]
        [InlineData(new string[] { "5", "5", "4", "2" }, "4")]
        [InlineData(new string[] { "4", "4", "4" }, "-1")]
        [InlineData(new string[] { }, "-1")]
        //Added test cases
        [InlineData(new string[] { "-7", "-7" }, "-1")]
        [InlineData(new string[] { "5", "5", "5", "-4", "-3" }, "-3")]
        [InlineData(new string[] { "5", "6", "4", "2" }, "5")]
        [InlineData(new string[] { "5", "6", "7", "8" }, "7")]
        [InlineData(new string[] { "-1", "-2", "-3", "-4" }, "-2")]
        [InlineData(new string[] { "-1", "-2", "-3", "9" }, "-1")]
        [InlineData(new string[] { "-1", "-2", "-3", "5", "6" }, "5")]
        [InlineData(new string[] { "-9", "-7", "-5", "-3" }, "-5")]
        [InlineData(new string[] { "4", "4", "4", "4", "4" }, "-1")]
        //Special test cases
        [InlineData(new string[] { "7" }, "-1")]
        [InlineData(new string[] { "a", "1", "2", "b" }, "-1")]
        [InlineData(new string[] { "d" }, "-1")]
        [InlineData(new string[] { "11111111111111111111", "11111111111111111112" }, "11111111111111111111")]
        public void GetSecondMaxValue_Test(string[] input, string expected)
        {
            var actualNumber = PPLib.GetSecondMaxValue(input);
            //Note: Need to change expected type to string
            //      because cannot run the test if expected type uses BigInteger
            var expectedNumber = BigInteger.Parse(expected);
            Assert.Equal(expectedNumber, actualNumber); 
        }
    }
}
