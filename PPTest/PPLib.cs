﻿using System;
using System.Linq;
using System.Numerics;

namespace PPTest
{
    public static class PPLib
    {
        public static BigInteger GetSecondMaxValue(string[] input)
        {
            var secondLargest = new BigInteger(-1);

            if (input.Length == 0 || input.Length == 1)
            {
                return secondLargest;
            }
            //If input contains non integer
            if (input.Any(x => !BigInteger.TryParse(x, out BigInteger number)))
            {
                return secondLargest;
            }

            var numbers = input.Select(x =>
            {
                return BigInteger.TryParse(x, out BigInteger number) ? number : 0;
            }
            ).ToArray();

            BigInteger largest = new BigInteger(-1);

            if (numbers[0] > numbers[1])
            {
                largest = numbers[0];
                secondLargest = numbers[1];
            }
            else if (numbers[0] < numbers[1])
            {
                largest = numbers[1];
                secondLargest = numbers[0];
            }
            else if (numbers[0] == numbers[1])
            {
                largest = numbers[0];
            }

            if (numbers.Length > 1)
            {
                for (int i = 2; i < numbers.Length; i++)
                {
                    if (largest == numbers[i])
                    {
                        continue;
                    }

                    var currentNumber = numbers[i];
                    //For handle case: ["5", "5", "5", "-4", "-3"]
                    if (currentNumber.Sign == -1 && largest > 0)
                    {
                        currentNumber *= -1;
                    }

                    if ((currentNumber <= largest) && currentNumber > secondLargest)
                    {
                        secondLargest = numbers[i];
                    }

                    if (currentNumber > largest)
                    {
                        secondLargest = largest;
                        largest = numbers[i];
                    }
                }
            }

            return secondLargest;
        }

        //Note: Get second max value from from an array of an integers
        //      Currently this method has O(N log N)
        public static BigInteger GetOldSecondMaxValue(string[] input)
        {
            var result = new BigInteger(-1);
            if (input.Length == 0 || input.Length == 1)
            {
                return result;
            }
            //If input contains non integer
            if (input.Any(x => !BigInteger.TryParse(x, out BigInteger number)))
            {
                return result;
            }

            var numbers = input.Select(x =>
            {
                return BigInteger.TryParse(x, out BigInteger number) ? number : 0;
            }
            ).OrderByDescending(x => x).ToArray();

            var firstNumber = numbers[0];
            for (int i = 1; i < numbers.Length; i++)
            {
                if (firstNumber == numbers[i])
                {
                    continue;
                }
                else if (firstNumber > numbers[i])
                {
                    return numbers[i];
                }
            }

            return result;
        }

    }
}
