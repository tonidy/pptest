### Install .NET Core

- Install .NET Core 3.0 -> https://dotnet.microsoft.com/download
- Make sure you can run `dotnet` in command prompt / terminal

Note:
- Maybe this article can help you -> https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/install

### Run test

```
> cd PPTest\Tests
> dotnet test -v normal
```
